<!DOCTYPE html>
<html >
<head>
    <base href="<?php bloginfo("template_directory"); ?>/">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Home</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
<!--    <link rel="stylesheet" href="assets/et-line-font-plugin/style.css">-->
<!--    <link rel="stylesheet" href="assets/bootstrap-material-design-font/css/material.css">-->
<!--    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic&subset=latin">-->
<!--    <link rel="stylesheet" href="assets/tether/tether.min.css">-->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/animate.css/animate.min.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
<!--    <link rel="stylesheet" href="assets/mobirise-gallery/style.css">-->
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="style.css?v1.0.0" type="text/css">
    <?php wp_head(); ?>
</head>
<body>
<section id="menu-0" data-rv-view="0">

    <nav class="navbar navbar-dropdown navbar-fixed-top bg-color">
        <div class="container">

            <div class="mbr-table">
                <div class="mbr-table-cell">

                    <div class="navbar-brand">
                        <a href="https://mobirise.com" class="navbar-logo"><img src="assets/images/logo.png" alt="Mobirise"></a>
                        <a class="navbar-caption" href="https://mobirise.com">MOBIRISE</a>
                    </div>

                </div>
                <div class="mbr-table-cell">

                    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="hamburger-icon"></div>
                    </button>

                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary',
                        'menu_id'        => 'exCollapsingNavbar',
                        'container'         => 'ul',
                        'depth'          => 3,
                        'menu_class'     => 'nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm',
                        'walker'         => new Bootstrap_NavWalker(),
                        'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
                    ) );
                    ?>

                    <button hidden="" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
                        <div class="close-icon"></div>
                    </button>

                </div>
            </div>

        </div>
    </nav>

</section>