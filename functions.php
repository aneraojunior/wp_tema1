<?php
show_admin_bar(false);
add_filter('widget_text', 'do_shortcode');

//Customizer
require('functions/customizer/customizer-service.php');
require('functions/customizer/customizer-header-contact.php');
require('functions/customizer/customizer-banner-one.php');
require('functions/customizer/customizer-count.php');
require('functions/customizer/customizer-practice-area.php');
require('functions/customizer/customizer-contact.php');

//Widgets
require( 'functions/widgets/sidebar.php');
require( 'functions/widgets/categories.php');
require( 'functions/widgets/posts_recentes.php');
require( 'functions/widgets/slider.php');

//Classes
require get_template_directory() . '/functions/classes/WP-Bootstrap-Navwalker.class.php';

register_nav_menus( array(
    'primary' => esc_html__( 'Primary', 'theme-textdomain' ),
) );

function cooprotec_setup()
{
    //Add Theme Support Title Tag
    add_theme_support( "title-tag" );

    require_once('theme_setup_data.php');
}
add_action( 'after_setup_theme', 'cooprotec_setup' );

/* Custom Logo */
add_theme_support( 'custom-logo' );
function themename_custom_logo_setup()
{
    $defaults = array(

        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'navbar-brand', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

/* Auto resumo */
function excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit)
    {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    }
    else
    {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

function content($limit)
{
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit)
    {
        array_pop($content);
        $content = implode(" ",$content).'...';
    }
    else
    {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/[.+]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function url()
{
    return $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

function redirect($link)
{
    echo '<script>location.href="'.$link.'"</script>';
}

function danger($mensagem)
{
    echo "<br><span id='msg'></span><br><div class='alert alert-danger' style=\"text-align: center;\">$mensagem</div>";
}

function success($mensagem)
{
    echo "<br><span id='msg'></span><br><div class='alert alert-success' style=\"text-align: center;\">$mensagem</div>";
}

function info($mensagem)
{
    echo "<br><span id='msg'></span><br><div class='alert alert-info' style=\"text-align: center;\">$mensagem</div>";
}

function warning($mensagem)
{
    echo "<br><span id='msg'></span><br><div class='alert alert-warning' style=\"text-align: center;\">$mensagem</div>";
}

// Set our theme version.
define( 'Coop', '1.0' );

if ( ! function_exists( 'generate_setup' ) ) {
    add_action( 'after_setup_theme', 'generate_setup' );
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * @since 0.1
     */
    function generate_setup() {
        // Make theme available for translation.
        load_theme_textdomain( 'generatepress' );

        // Add theme support for various features.
        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link', 'status' ) );
        add_theme_support( 'woocommerce' );
        add_theme_support( 'title-tag' );
        add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
        add_theme_support( 'customize-selective-refresh-widgets' );

        add_theme_support( 'custom-logo', array(
            'height' => 70,
            'width' => 350,
            'flex-height' => true,
            'flex-width' => true
        ) );

        // Register primary menu.
        register_nav_menus( array(
            'primary' => __( 'Primary Menu', 'generatepress' ),
        ) );

        /**
         * Set the content width to something large
         * We set a more accurate width in generate_smart_content_width()
         */
        global $content_width;
        if ( ! isset( $content_width ) ) {
            $content_width = 1200; /* pixels */
        }

        // This theme styles the visual editor to resemble the theme style.
        add_editor_style( 'css/admin/editor-style.css' );
    }
}

/**
 * Theme files
 */
require get_template_directory() . '/inc/theme-functions.php';
require get_template_directory() . '/inc/defaults.php';
require get_template_directory() . '/inc/general.php';


if ( is_admin() )
{
    require get_template_directory() . '/inc/meta-box.php';
    require get_template_directory() . '/inc/dashboard.php';
}


?>