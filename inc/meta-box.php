<?php
/**
 * Builds our main Layout meta box.
 *
 * @package GeneratePress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action( 'admin_enqueue_scripts', 'generate_enqueue_meta_box_scripts' );
/**
 * Adds any scripts for this meta box.
 *
 * @since 2.0
 *
 * @param string $hook The current admin page.
 */
function generate_enqueue_meta_box_scripts( $hook ) {
	if ( in_array( $hook, array( 'post.php', 'post-new.php' ) ) ) {
		$post_types = get_post_types( array( 'public' => true ) );
		$screen = get_current_screen();
		$post_type = $screen->id;

		if ( in_array( $post_type, ( array ) $post_types ) ) {
			wp_enqueue_style( 'generate-layout-metabox', get_template_directory_uri() . '/css/admin/meta-box.css', array(), GENERATE_VERSION );
		}
	}
}

add_action( 'add_meta_boxes', 'generate_register_layout_meta_box' );
/**
 * Generate the layout metabox
 *
 * @since 2.0
 */
function generate_register_layout_meta_box() {
	if ( ! current_user_can( apply_filters( 'generate_metabox_capability', 'edit_theme_options' ) ) ) {
		return;
	}

	if ( ! defined( 'GENERATE_LAYOUT_META_BOX' ) ) {
		define( 'GENERATE_LAYOUT_META_BOX', true );
	}

	$post_types = get_post_types( array( 'public' => true ) );
	foreach ($post_types as $type) {
		if ( 'attachment' !== $type ) {
			add_meta_box (
				'generate_layout_options_meta_box',
				esc_html__( 'Layout', 'generatepress' ),
				'generate_do_layout_meta_box',
				$type,
				'normal',
				'high'
			);
		}
	}
}

/**
 * Build our meta box.
 *
 * @since 2.0
 *
 * @param object $post All post information.
 */
function generate_do_layout_meta_box( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'generate_layout_nonce' );
	$stored_meta = get_post_meta( $post->ID );
	$stored_meta['_generate-sidebar-layout-meta'][0] = ( isset( $stored_meta['_generate-sidebar-layout-meta'][0] ) ) ? $stored_meta['_generate-sidebar-layout-meta'][0] : '';
	$stored_meta['_generate-footer-widget-meta'][0] = ( isset( $stored_meta['_generate-footer-widget-meta'][0] ) ) ? $stored_meta['_generate-footer-widget-meta'][0] : '';
	$stored_meta['_generate-full-width-content'][0] = ( isset( $stored_meta['_generate-full-width-content'][0] ) ) ? $stored_meta['_generate-full-width-content'][0] : '';
	$stored_meta['_generate-disable-headline'][0] = ( isset( $stored_meta['_generate-disable-headline'][0] ) ) ? $stored_meta['_generate-disable-headline'][0] : '';
	?>
	<script>
		jQuery(document).ready(function($) {
			$( '.generate-meta-box-menu li a' ).on( 'click', function( event ) {
				event.preventDefault();
				$( this ).parent().addClass( 'current' );
				$( this ).parent().siblings().removeClass( 'current' );
				var tab = $( this ).attr( 'href' );
				$( '.generate-meta-box-content' ).children( 'div' ).not( tab ).css( 'display', 'none' );
				$( tab ).fadeIn( 100 );
			});
		});
	</script>
	<div id="generate-meta-box-container">
		<ul class="generate-meta-box-menu">
			<?php if ( ! defined( 'GENERATE_DE_VERSION' ) || defined( 'GENERATE_DE_LAYOUT_META_BOX' ) ) : ?>
				<li class="current"><a href="#generate-layout-disable-elements"><?php esc_html_e( 'Disable Elements', 'generatepress' ); ?></a></li>
			<?php endif; ?>
			<?php do_action( 'generate_layout_meta_box_menu_item' ); ?>
		</ul>
		<div class="generate-meta-box-content">
			<div id="generate-layout-disable-elements">
				<?php if ( ! defined( 'GENERATE_DE_VERSION' ) ) : ?>
					<div class="generate_disable_elements">
						<label for="meta-generate-disable-headline" style="display:block;margin: 0 0 1em;" title="<?php esc_attr_e( 'Content Title', 'generatepress' );?>">
							<input type="checkbox" name="_generate-disable-headline" id="meta-generate-disable-headline" value="true" <?php checked( $stored_meta['_generate-disable-headline'][0], 'true' ); ?>>
							<?php esc_html_e( 'Content Title', 'generatepress' );?>
						</label>
					</div>
				<?php endif; ?>

				<?php do_action( 'generate_layout_disable_elements_section', $stored_meta ); ?>
			</div>
			<?php do_action( 'generate_layout_meta_box_content', $stored_meta ); ?>
		</div>
	</div>
    <?php
}

add_action( 'save_post', 'generate_save_layout_meta_data' );

function generate_save_layout_meta_data( $post_id ) {
	$is_autosave = wp_is_post_autosave( $post_id );
	$is_revision = wp_is_post_revision( $post_id );
	$is_valid_nonce = ( isset( $_POST[ 'generate_layout_nonce' ] ) && wp_verify_nonce( sanitize_key( $_POST[ 'generate_layout_nonce' ] ), basename( __FILE__ ) ) ) ? true : false;

	if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	// We only need this if the Disable Elements module doesn't exist
		$disable_content_title_key   = '_generate-disable-headline';
		$disable_content_title_value = filter_input( INPUT_POST, $disable_content_title_key, FILTER_SANITIZE_STRING );

		if ( $disable_content_title_value ) {
			update_post_meta( $post_id, $disable_content_title_key, $disable_content_title_value );
		} else {
			delete_post_meta( $post_id, $disable_content_title_key );
		}


	do_action( 'generate_layout_meta_box_save', $post_id );
}
