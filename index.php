<?php get_header(); ?>
    <?php if ( function_exists( 'soliloquy' ) ) { ?>
        <section class="soliloquy">
            <?php dynamic_sidebar('index-soliloquy-slider'); ?>
        </section>
    <?php } else { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-lg-center">
                    <Br>
                    Your custom content below
                    <br><Br><br><br><Br>
                </h1>
            </div>
        </div>
    </div>
    <?php } ?>
<?php get_footer(); ?>